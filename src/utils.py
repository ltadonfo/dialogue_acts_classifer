from transformers import BertTokenizer

bert_tokenizer = None

def get_bert_tokenizer():
    return bert_tokenizer


def init_bert_tokenizer():
    global bert_tokenizer
    bert_tokenizer = BertTokenizer.from_pretrained('bert-base-uncased')
    return bert_tokenizer