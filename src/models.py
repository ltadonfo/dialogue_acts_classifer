import wandb
import tensorflow as tf
from keras import Sequential, Input
from keras.models import Model
from keras.layers import Bidirectional, TimeDistributed, Embedding, LSTM, Dropout, Dense, Concatenate,  GlobalAveragePooling1D, Flatten
from keras import layers
#ModelWithCRFLoss
from keras import backend as k
from tensorflow_addons.layers import CRF
from tf2crf import ModelWithCRFLoss

from src.data_loader import get_labels, get_max_len


class BiRNN_CRF_factory:
    def __init__(self, embedding_matrix, max_nr_utterances, n_tags):
        self.embedding_matrix = embedding_matrix
        self.n_tags = n_tags
        self.max_nr_utterances = max_nr_utterances

    def get(self):
        return build_bilstm_crf_model(self.embedding_matrix, self.max_nr_utterances, self.n_tags)


def build_bilstm_crf_model(embedding_matrix, max_nr_utterances, n_tags, verbose=True):
    print("loading model...")
    # dropout_rate = wandb.config.dropout
    # nr_lstm_cells = wandb.config.hidden_layer_sizes
    # init_lr = config.model["init_lr"]
    # decay_steps = config.model["decay_steps"]
    # decay_rate = config.model["decay_rate"]

    embedding_layer = Embedding(
        embedding_matrix.shape[0],
        embedding_matrix.shape[1],
        weights=[embedding_matrix],
        input_length=300,
        trainable=True,
    )

    utterance_encoder = Sequential()
    utterance_encoder.add(embedding_layer)

    # average pooling
    # utterance_encoder.add(Bidirectional(LSTM(300, return_sequences=True)))
    # utterance_encoder.add(AveragePooling1D(max_nr_words))

    # last pooling
    utterance_encoder.add(Bidirectional(LSTM(wandb.config.hidden_layer_sizes)))
    utterance_encoder.add(Dropout(wandb.config.dropout))
    # utterance_encoder.add(Flatten())
    if verbose:
        print
        # utterance_encoder.summary()

    crf = CRF(units=n_tags)
    x_input = Input(shape=(max_nr_utterances, 300))
    h = TimeDistributed(utterance_encoder)(x_input)

    h = Bidirectional(LSTM(wandb.config.hidden_layer_sizes, return_sequences=True))(h)
    h = Dropout(wandb.config.dropout)(h)
    h = Dense(n_tags, activation=None)(h)
    crf_output = crf(h)
    base_model = Model(x_input, crf_output)
    # model = Model(x_input, crf_output)
    model = ModelWithCRFLoss(base_model, sparse_target=True)
    model.compile(optimizer=tf.keras.optimizers.Adam(wandb.config.learning_rate))
    print("Done!")
    return model


def build_bert_model(transformer, max_nr_utterances, n_tags, verbose=True):
    # max_nr_words = config.data["max_nr_words"]
    # nr_lstm_cells = wandb.config.hidden_layer_sizes
    # dropout_rate = wandb.config.dropout
    """""""""
    Template for building a model off of the BERT or DistilBERT architecture
    for a binary classification task.

    Input:
      - transformer:  a base Hugging Face transformer model object (BERT or DistilBERT)
                      with no added classification head attached.
      - max_length:   integer controlling the maximum number of encoded tokens 
                      in a given sequence.

    Output:
      - model:        a compiled tf.keras.Model with added classification layers 
                      on top of the base pre-trained model architecture.
    """""""""
    print("Loading model...")
    input_ids_layer = tf.keras.layers.Input(shape=(max_nr_utterances, 768),
                                            name='input_ids',
                                            dtype='int32')
    input_attention_layer = tf.keras.layers.Input(shape=(max_nr_utterances, 768,),
                                                  name='input_attention',
                                                  dtype='int32')
    cls_token = []
    for (i, j) in zip(range(input_ids_layer.shape[1]), range(input_attention_layer.shape[1])):
        last_hidden_state = transformer([input_ids_layer[:, i, :], input_attention_layer[:, j, :]])[0]

        # We only care about output for the [CLS] token,
        # which is located at index 0 of every encoded sequence.
        # Splicing out the [CLS] tokens gives us 2D data.
        temp_cls_token = last_hidden_state[:, 0, :]
        cls_token.append(temp_cls_token)
        # cls_token = Concatenate(axis=0)([cls_token, temp_cls_token])

    cls_token = Concatenate(axis=1)(cls_token)
    cls_token = tf.reshape(cls_token, [-1, max_nr_utterances, 768])

    crf = CRF(units=n_tags)
    h = Bidirectional(LSTM(wandb.config.hidden_layer_sizes, return_sequences=True))(tf.convert_to_tensor(cls_token))
    h = Dropout(wandb.config.dropout)(h)
    h = Dense(n_tags, activation=None)(h)
    crf_output = crf(h)
    # model = Model([input_ids_layer, input_attention_layer], crf_output)
    base_model = Model([input_ids_layer, input_attention_layer], crf_output)
    # model = Model(x_input, crf_output)
    model = ModelWithCRFLoss(base_model, sparse_target=True)
    model.compile(optimizer=tf.keras.optimizers.Adam(wandb.config.learning_rate),  metrics=[model.metric])
    print("Done!")
    return model


# Add attention layer to the deep learning network
class attention(layers.Layer):
    def __init__(self, **kwargs):
        super(attention, self).__init__(**kwargs)

    def build(self, input_shape):
        self.W = self.add_weight(name='attention_weight', shape=(input_shape[-1], 1),
                                 initializer='random_normal', trainable=True)
        self.b = self.add_weight(name='attention_bias', shape=(input_shape[1], 1),
                                 initializer='zeros', trainable=True)
        super(attention, self).build(input_shape)

    def call(self, x):
        # Alignment scores. Pass them through tanh function
        e = k.tanh(k.dot(x, self.W) + self.b)
        # Remove dimension of size 1
        e = k.squeeze(e, axis=-1)
        # Compute the weights
        alpha = k.softmax(e)
        # Reshape to tensorFlow format
        alpha = k.expand_dims(alpha, axis=-1)
        # Compute the context vector
        context = x * alpha
        context = k.sum(context, axis=1)
        return context



class TransformerBlock(layers.Layer):
    def __init__(self, embed_dim, num_heads, ff_dim, rate=0.5):
        super(TransformerBlock, self).__init__()
        self.att = layers.MultiHeadAttention(num_heads=num_heads, key_dim=embed_dim)
        self.ffn = Sequential([layers.Dense(ff_dim, activation="relu"), layers.Dense(embed_dim), ])
        self.layernorm1 = layers.LayerNormalization(epsilon=1e-6)
        self.layernorm2 = layers.LayerNormalization(epsilon=1e-6)
        self.dropout1 = layers.Dropout(rate)
        self.dropout2 = layers.Dropout(rate)

    def call(self, inputs, training):
        attn_output = self.att(inputs, inputs)
        attn_output = self.dropout1(attn_output, training=training)
        out1 = self.layernorm1(inputs + attn_output)
        ffn_output = self.ffn(out1)
        ffn_output = self.dropout2(ffn_output, training=training)
        out = self.layernorm2(out1 + ffn_output)
        return out


def bert_without_crf(model):
    input_ids = tf.keras.layers.Input(shape=(get_max_len(), ), name='input_ids', dtype='int32')
    attention_mask = tf.keras.layers.Input(shape=(get_max_len(), ), name='attention_mask', dtype='int32')

    outputs = model(input_ids=input_ids, attention_mask=attention_mask)
    # Extract the last hidden state of the token `[CLS]` for classification task
    last_hidden_state_cls = outputs[0]

    if wandb.config.bilstm_layer is True:
        bilstm = Bidirectional(LSTM(units=wandb.config.hidden_layer_sizes, return_sequences=wandb.config.attention))(last_hidden_state_cls)
    else:
        bilstm = last_hidden_state_cls

    if wandb.config.attention is True:
        transformer_block = TransformerBlock(embed_dim=wandb.config.hidden_layer_sizes*2 if wandb.config.bilstm_layer is True else 768, num_heads=len(get_labels()), ff_dim=wandb.config.ff_dim)
        att = transformer_block(bilstm)
        bilstm = GlobalAveragePooling1D()(att)

    if wandb.config.bilstm_layer is False & wandb.config.attention is False:
        flatten = Flatten()(bilstm)
        bilstm = Dense(384, activation=None)(flatten)
    final_model_output = Dense(len(get_labels()), activation='softmax')(bilstm)

    model = Model([input_ids, attention_mask], final_model_output)

    model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
    return model
