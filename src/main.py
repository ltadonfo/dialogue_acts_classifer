# This is a sample Python script.
import random
import tensorflow as tf
from transformers import TFBertModel
from wandb.integration.keras import WandbMetricsLogger, WandbModelCheckpoint

from models import BiRNN_CRF_factory, build_bert_model, bert_without_crf
from src.data_loader import build_conv_data, get_labels, data_without_crf
from utils import init_bert_tokenizer
from crfutils import init_tokenizer, get_embedding_matrix
import argparse
import wandb

gpus = tf.config.list_physical_devices('GPU')
if gpus:
  # Restrict TensorFlow to only use the first GPU
  try:
    tf.config.set_visible_devices(gpus[0], 'GPU')
    logical_gpus = tf.config.list_logical_devices('GPU')
    print(len(gpus), "Physical GPUs,", len(logical_gpus), "Logical GPU")
  except RuntimeError as e:
    # Visible devices must be set before GPUs have been initialized
    print(e)


crf_sweep_config = {
            'method': 'random',  # grid, random
            'metric': {
                'name': 'val_loss',
                'goal': 'minimize'
            },
            'parameters': {
                'learning_rate': {
                    'values': [5e-5, 3e-5, 2e-5]
                },
                'batch_size': {
                    'values': [4, 8, 16, 32]
                },
                'epochs': {
                    'values': [2, 3, 4, 5]
                },
                'hidden_layer_sizes': {
                    'values': [256, 512]
                },
                'dropout': {
                    'values': [0.3, 0.4, 0.5]
                },
                'max_nr_utterances': {
                    'values': [5, 10, 25, 50]
                },
                'tag_level': {
                    'values': [0, 1, 2]
                },
                'model_type': {
                    'values': ['bert', 'CoNNb', 'without_crf']
                }

            }
        }

sweep_config = {
            'method': 'random',  # grid, random
            'metric': {
                'name': 'val_loss',
                'goal': 'minimize'
            },
            'parameters': {
                'learning_rate': {
                    'values': [5e-5, 3e-5, 2e-5]
                },
                'batch_size': {
                    'values': [8, 16, 32, 64]
                },
                'epochs': {
                    'values': [3, 4, 5]
                },
                'hidden_layer_sizes': {
                    'values': [128]
                },
                'dropout': {
                    'values': [0.3, 0.4, 0.5]
                },
                'max_nr_utterances': {
                    'values': [5, 10, 25, 50]
                },
                'tag_level': {
                    'values': [0, 1, 2]
                },
                'ff_dim': {
                    'values': [64]
                },
                'ctx': {
                    'values': [False]
                },
                'bilstm_layer': {
                    'values': [False]
                },
                'attention': {
                    'values': [True]
                },
                'num_heads': {
                    'values': [2, 4, 8, 16]
                }


            }
        }

def train_crf():
    wandb.init()
    if wandb.config.model_type == 'bert':
        x_train, y_train, x_val, y_val = build_conv_data(test_d=False, label_level=wandb.config.tag_level, nb_utt=wandb.config.max_nr_utterances, encoding_type='bert')
        model = TFBertModel.from_pretrained("bert-base-uncased")
        model.trainable = False
        model = build_bert_model(model, wandb.config.max_nr_utterances, len(get_labels()))
    else:
        x_train, x_val, y_train, y_val = build_conv_data(test_d=False, label_level=wandb.config.tag_level, nb_utt=wandb.config.max_nr_utterances, encoding_type='CoNNb')
        embedding_matrix = get_embedding_matrix()
        model_factory = BiRNN_CRF_factory(embedding_matrix, wandb.config.max_nr_utterances, len(get_labels()))
        model = model_factory.get()
    model.fit(
        x_train,
        y_train,
        batch_size=wandb.config.batch_size,
        epochs=wandb.config.epochs,
        validation_data=(x_val, y_val),
        callbacks=[WandbMetricsLogger(log_freq='epoch'), WandbModelCheckpoint("models", monitor='val_accuracy', save_best_only=True)],
    )


def train():
    wandb.init()
    x_train, x_val, y_train, y_val = data_without_crf(test_d=False, label_level=wandb.config.tag_level)
    model = TFBertModel.from_pretrained("bert-base-uncased")
    model.trainable = False
    model = bert_without_crf(model)
    model.fit(
        x_train,
        y_train,
        batch_size=wandb.config.batch_size,
        epochs=wandb.config.epochs,
        validation_data=(x_val, y_val),
        callbacks=[WandbMetricsLogger(log_freq='epoch'), WandbModelCheckpoint("models", monitor='val_accuracy', save_best_only=True)],
    )
# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    wandb.login()
    argParser = argparse.ArgumentParser()
    argParser.add_argument("-c", "--crf", type=bool, help="whether training models with CRF as last layer or not", default=False)
    args = argParser.parse_args()
    if args.crf is True:
        cfr_sweep_id = wandb.sweep(crf_sweep_config, project='Dialogue_Acts_Classifier_with_CRF')
        wandb.agent(cfr_sweep_id, function=train_crf)
    else:
        sweep_id = wandb.sweep(sweep_config, project='Dialogue_Acts_Classifier')
        wandb.agent(sweep_id, function=train)








