import keras.utils
import nltk
import numpy as np
import pandas as pd
import tensorflow as tf
import torch
import wandb
from keras.utils import pad_sequences, to_categorical
from transformers import BertTokenizer
from tqdm import tqdm

tqdm.pandas()
from src.crfutils import *

from utils import init_bert_tokenizer

# De la granularité la plus fine (0) à la moins fine (2)
level_to_label = {
    0: 'label_level_0',
    1: 'label_level_2',
    2: 'label_level_1'
}

labels, c_tokenizer, bert_tokenizer, max_nr_words, max_len = None, None, None, 0, 0


class ConceptNetDict:
    def __init__(self):
        path = config.paths["embeddings"] + "en_mini_conceptnet.h5"
        self.df = pd.read_hdf(path, "data")

    def __getitem__(self, idx):
        return self.df.loc[idx].values

    def __contains__(self, idx):
        return self.get(idx) is not None

    def get(self, key):
        try:
            return self.__getitem__(key)
        except KeyError:
            return


def load_pretrained_conceptnet():
    return ConceptNetDict()


class DialogueActDataset(torch.utils.data.Dataset):
    def __init__(self, encodings):
        self.encodings = encodings

    def __getitem__(self, idx):
        return {key: torch.tensor(val[idx]) for key, val in self.encodings.items()}

    def __len__(self):
        return self.encodings.input_ids.shape[0]


def get_labels():
    return labels

def get_max_len():
    return max_len

def get_max_nr_words():
    return max_nr_words


def refine_data(data, level=0, train_data=False):
    global labels, max_nr_words
    data = data.query("label != 'Tc'  & label_level_1 != 'Tc' & label_level_2 != 'Tc'")
    for tag in ['label_level_0', 'label_level_1', 'label_level_2']:
        data[tag] = data['label' if tag == 'label_level_0' else tag].progress_apply(lambda x: 'Misc' if x == 'M' else ('Hy' if x == 'Mb' else x))
        # data[tag] = data[tag].progress_apply(lambda x: 'Hy' if x == 'Mb' else x)
    if train_data is True:
        labels = list(set(data[level_to_label[level]].tolist()))
        labels.sort()
        max_nr_words = max(data['text'].apply(lambda x: len(x.strip().split())).tolist())
    data['label'] = data[level_to_label[level]].progress_apply(lambda x: labels.index(x))
    return data


def encode_data(data, tokenizer):
    inputs = tokenizer.batch_encode_plus(data['text'], add_special_tokens=True, max_length=max_nr_words, padding='max_length',
                                         return_attention_mask=True, truncation=True, return_tensors='pt')
    inputs['labels'] = torch.LongTensor(data['label'].values.tolist())
    return input


def get_data(level=0, test=False):
    train_data = pd.read_csv('../Data/MRDA/Filtered/train_data.csv', sep=';', encoding='utf-8')[:1000]
    train_data = refine_data(train_data, level=level, train_data=True)
    if wandb.config.ctx is True:
        train_data['text_2'] = train_data.text.shift(1)
        train_data['text'] = train_data.progress_apply(lambda x: '[CLS]' + (x['text_2'] + '[SEP]' if not pd.isna(x['text_2']) else '') + x.text + '[SEP]', axis=1)
    define_max_length(train_data['text'].tolist())

    if test is True:
        test_data = pd.read_csv('../Data/MRDA/Filtered/test_data.csv', sep=';', encoding='utf-8')
        test_data = refine_data(test_data, level=level)
        if wandb.config.ctx is True:
            test_data['text_2'] = test_data.text.shift(1)
            test_data['text'] = test_data.progress_apply(lambda x: '[CLS]' + (x['text_2'] + '[SEP]' if not pd.isna(x['text_2']) else '') + x.text + '[SEP]', axis=1)
        return test_data
    else:
        val_data = pd.read_csv('../Data/MRDA/Filtered/val_data.csv', sep=';', encoding='utf-8')[:100]
        val_data = refine_data(val_data, level=level)
        if wandb.config.ctx is True:
            val_data['text_2'] = val_data.text.shift(1)
            val_data['text'] = val_data.progress_apply(lambda x: '[CLS]' + (x['text_2'] + '[SEP]' if not pd.isna(x['text_2']) else '') + x.text + '[SEP]', axis=1)
        return train_data, val_data


def create_conversations(data, max_nr_utterances=5, encoding_type='bert'):
    conversations = chunk(data['text'].tolist(), max_nr_utterances)
    conv_labels = chunk(data['label'].tolist(), max_nr_utterances)

    # encode data whether with BERT or ConceptNet Numberbacth(CoNNb)
    if encoding_type == 'bert':
        y = make_model_readable_y(conv_labels, max_nr_utterances)
        # conversation_ids, conversation_attention = compute_input_ids_and_attentions(conversations, name='X_train')
        input_ids, attention_mask = encode_with_bert(conversations, max_nr_utterances)
        return input_ids, attention_mask, y
    else:
        return make_model_readable_data(
            conversations, c_tokenizer, conv_labels, max_nr_utterances
        )


def define_max_length(data):
    global max_len, bert_tokenizer
    bert_tokenizer = init_bert_tokenizer()
    # Encode our concatenated data
    encoded = [bert_tokenizer.encode(sent, add_special_tokens=True) for sent in data]

    # Find the maximum length
    max_len = max([len(sent) for sent in encoded])
    print('Max length: ', max_len)

def encode_label(labels):
    encoded_labels = to_categorical(labels, num_classes=len(get_labels()))
    return encoded_labels

def data_without_crf(test_d=False, label_level=0):
    if test_d is True:
        test_data = get_data(level=label_level, test=True)
        inputs = bert_tokenizer(test_data['text'].tolist(), add_special_tokens=not wandb.config.ctx, padding='max_length', max_length=max_len, truncation=True, return_tensors="tf")
        return tf.convert_to_tensor(inputs['input_ids']), tf.convert_to_tensor(inputs['attention_mask']), encode_label(test_data['label'].tolist())
    else:
        train_data, val_data = get_data(level=label_level, test=False)
        train_inputs = bert_tokenizer(train_data['text'].tolist(), add_special_tokens=not wandb.config.ctx, padding='max_length', max_length=max_len, truncation=True, return_tensors="tf")
        train_ids, train_atts, train_labels = tf.convert_to_tensor(train_inputs['input_ids']), tf.convert_to_tensor(train_inputs['attention_mask']), encode_label(train_data['label'].tolist())

        val_inputs = bert_tokenizer(val_data['text'].tolist(), add_special_tokens=not wandb.config.ctx, padding='max_length', max_length=max_len, truncation=True, return_tensors="tf")
        val_ids, val_atts, val_labels = tf.convert_to_tensor(val_inputs['input_ids']), tf.convert_to_tensor(val_inputs['attention_mask']), encode_label(val_data['label'].tolist())

        return [train_ids, train_atts], [val_ids, val_atts], train_labels, val_labels

def build_conv_data(test_d=False, label_level=0, nb_utt=5, encoding_type='bert'):
    global c_tokenizer
    if encoding_type == 'bert':
        if test_d is True:
            test_data = get_data(level=label_level, test=True)
            test_input_ids, test_atts, y_test = create_conversations(test_data, max_nr_utterances=nb_utt, encoding_type=encoding_type)
            return [test_input_ids, test_atts], y_test
        else:
            train_data, val_data = get_data(level=label_level, test=False)
            train_input_ids, train_atts, y_train = create_conversations(train_data, max_nr_utterances=nb_utt, encoding_type=encoding_type)
            val_input_ids, val_atts, y_val = create_conversations(train_data, max_nr_utterances=nb_utt, encoding_type=encoding_type)
            return [train_input_ids, train_atts], y_train, [val_input_ids, val_atts], y_val
    else:
        c_tokenizer = init_tokenizer()
        if test_d is True:
            test_data = get_data(level=label_level, test=True)
            return create_conversations(test_data, max_nr_utterances=nb_utt, encoding_type=encoding_type)
        else:
            train_data, val_data = get_data(level=label_level, test=False)
            x_train, y_train = create_conversations(train_data, max_nr_utterances=nb_utt, encoding_type=encoding_type)
            x_val, y_val = create_conversations(val_data, max_nr_utterances=nb_utt, encoding_type=encoding_type)
            return x_train, x_val, y_train, y_val


def batch_encode(bert_tokenizer, texts):
    """""""""
    A function that encodes a batch of texts and returns the texts'
    corresponding encodings and attention masks that are ready to be fed 
    into a pre-trained transformer model.

    Input:
        - tokenizer:   Tokenizer object from the PreTrainedTokenizer Class
        - texts:       List of strings where each string represents a text
        - batch_size:  Integer controlling number of texts in a batch
        - max_length:  Integer controlling max number of words to tokenize in a given text
    Output:
        - input_ids:       sequence of texts encoded as a tf.Tensor object
        - attention_mask:  the texts' attention mask encoded as a tf.Tensor object
    """""""""

    input_ids = []
    attention_mask = []
    '''
        for i in range(0, 1, batch_size):
        batch = texts[i:i + batch_size]
        inputs = tokenizer.batch_encode_plus(batch,
                                             max_length=max_length,
                                             add_special_tokens=True,  # Add '[CLS]' and '[SEP]'
                                             return_token_type_ids=False,
                                             truncation=True,
                                             padding='max_length',
                                             return_attention_mask=True,
                                             return_tensors='tf',
                                             )
        input_ids.extend(inputs['input_ids'])
        attention_mask.extend(inputs['attention_mask'])
    '''
    inputs = bert_tokenizer.batch_encode_plus([str(txt) for txt in texts],  # Sentence to encode.
                                              add_special_tokens=True,  # Add '[CLS]' and '[SEP]'
                                              max_length=256,  # Pad & truncate all sentences.
                                              return_attention_mask=True,  # Construct attn. masks.
                                              truncation=True,
                                              padding='max_length',
                                              return_token_type_ids=False,
                                              return_tensors='tf',  # Return tensorflow tensors.
                                              )

    return tf.convert_to_tensor(inputs['input_ids']), tf.convert_to_tensor(inputs['attention_mask'])


def encode_with_bert(data, max_nr_sentences, model_dim=768, embeddings='bert'):
    if embeddings == 'bert':
        bert_tokenizer = init_bert_tokenizer()
        i_ids = np.zeros((len(data), max_nr_sentences, model_dim), dtype="int32")
        atts = np.zeros((len(data), max_nr_sentences, model_dim), dtype="int32")
        for i, sequence in enumerate(data):
            res = batch_encode(bert_tokenizer, sequence)
            for j, (iids, att) in enumerate(zip(res[0], res[1])):
                if j < model_dim:
                    if len(iids) > model_dim:
                        # print("WARNING: utterance too long, will be truncated,
                        # increase max_nr_words!")
                        iids = iids[:model_dim]
                        att = att[:model_dim]
                    i_ids[i, j, : len(iids)] = iids
                    atts[i, j, : len(att)] = att
        return i_ids, atts
    else:
        print()
        '''
        outputs =  np.zeros((len(data), max_nr_sentences, model_dim), dtype="int32")
        model = SentenceTransformer('all-MiniLM-L6-v2')
        # Sentences are encoded by calling model.encode()
        for i, sequence in tqdm(enumerate(data)):
             res = model.encode(sequence)
             for j, iids in enumerate(res):
                 outputs[i, j, : len(iids)] = iids
        return outputs
        '''


def pad_nested_sequences(sequences, max_nr_sentences, max_nr_words):
    X = np.zeros((len(sequences), max_nr_sentences, max_nr_words), dtype="int32")
    for i, sequence in enumerate(sequences):
        for j, utterance in enumerate(sequence):
            if j < max_nr_words:
                if len(utterance) > max_nr_words:
                    # print("WARNING: utterance too long, will be truncated,
                    # increase max_nr_words!")
                    utterance = utterance[:max_nr_words]
                X[i, j, : len(utterance)] = utterance
    return X


def make_model_readable_data(
        conversations, c_tokenizer, tags, max_nr_utterances
):
    X = make_model_readable_X(conversations, c_tokenizer, max_nr_utterances)
    y = make_model_readable_y(tags, max_nr_utterances)
    return X, y


def make_model_readable_X(conversations, c_tokenizer, max_nr_utterances):
    conversations = [
        [" ".join(nltk.word_tokenize(u)) for u in c] for c in conversations
    ]  # separates full stops etc
    conversation_sequences = [c_tokenizer.texts_to_sequences(c) for c in conversations]
    return pad_nested_sequences(conversation_sequences, max_nr_utterances, 300)


def make_model_readable_y(tags, max_nr_utterances):
    y = [[t_id for t_id in t_ids] for t_ids in tags]
    return pad_sequences(y, max_nr_utterances, padding="post")
