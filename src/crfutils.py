import os
import pickle
import numpy as np
from keras_preprocessing.text import Tokenizer
from data_loader import *


import config


tokenizer = word2id= None

def get_tokenizer():
    return tokenizer
def chunk(l_of_ls, chunk_size):
    chunked = split_into_chunks(l_of_ls, chunk_size)
    return chunked
    #return sum(chunked, [])


def split_into_chunks(l, chunk_size):
    res = [l[i : i + chunk_size] for i in range(0, len(l), chunk_size)]
    return res


def init_tokenizer(rebuild_from_all_words=False):
    global tokenizer
    tokenizer = Tokenizer(filters="")
    preloaded_exists = os.path.exists("./helper_files/tokenizer.pkl")
    if rebuild_from_all_words or not preloaded_exists:
        print("Building tokenizer from all words, this might take a while...")
        #all_words = get_all_words()
        #tokenizer.fit_on_texts(all_words)
        with open("../helper_files/tokenizer.pkl", "wb") as f:
            pickle.dump(tokenizer, f)
    else:
        print("Found prebuilt tokenizer, loading...")
        with open("./helper_files/tokenizer.pkl", "rb") as f:
            tokenizer = pickle.load(f)
    #word2id = tokenizer.word_index
    print("Done!")
    return tokenizer


def get_embedding_matrix(force_rebuild=False):
    fpath = "./helper_files/embedding_matrix.pkl"
    if not force_rebuild and os.path.exists(fpath):
        with open(fpath, "rb") as f:
            matrix = pickle.load(f)
    else:
        # glv_vector = load_pretrained_glove(path)
        glv_vector = load_pretrained_conceptnet()
        dim = config.data["glv_embedding_dim"]
        matrix = np.zeros((len(word2id) + 1, dim))

        for word, label in word2id.items():
            try:
                matrix[label] = glv_vector[word]
            except KeyError:
                continue
        with open(fpath, "wb") as matrix_file:
            pickle.dump(matrix, matrix_file)
    return matrix


